#!/bin/bash

docker builder prune -af
docker build . -t ansible:202302

echo "alias ad-create='docker run --name ansible-docker -d -v $PWD:/home -it ansible:202302'" >> ~/.zshrc
echo "alias ad='docker exec -it ansible-docker bash'">> ~/.zshrc
echo "alias ad-stop='docker stop ansible-docker'">> ~/.zshrc
echo "alias ad-start='docker start ansible-docker'">> ~/.zshrc

omz reload