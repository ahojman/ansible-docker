FROM ubuntu:22.10

RUN rm -rf /var/lib/apt/lists/*
RUN apt-get update -o Acquire::CompressionTypes::Order::=gz

RUN apt-get install -qq -y \ 
    apt-transport-https \
    virtualenv \
    ca-certificates \
    python3-pip \
    curl

RUN apt-get install -qq -y \
    python3-setuptools \
    jq \
    git \
    software-properties-common 

RUN pip3 install --upgrade pip; \
    pip3 install ansible; \
    ansible-galaxy collection install azure.azcollection; \
    pip3 install -r ~/.ansible/collections/ansible_collections/azure/azcollection/requirements-azure.txt

WORKDIR /home
